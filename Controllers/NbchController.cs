using System;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Text;
using System.IO.Compression;
using Microsoft.AspNetCore.Http;

namespace BkiScoring.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NbchController : ControllerBase
    {
        private readonly IConfiguration Configuration;

        public NbchController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        [HttpGet]
        public FileContentResult NbchGet()
        {
            string[] files = Directory.GetFiles(Configuration["RKK_NbchResponcePath"]);

            DateTime date = DateTime.Now;
            string stringDate = date.ToString("yyyyMMdd") + "_" + date.ToString("hhmmss") + "_Reply.xml";

            System.IO.File.Move(files[0], files[0].Remove(files[0].IndexOf('_') + 1) + stringDate);
            
            string[] filesNew = Directory.GetFiles(Configuration["RKK_NbchResponcePath"]);

            byte[] arrayRead;
            using (FileStream fstream = System.IO.File.OpenRead(filesNew[0]))
            {
                arrayRead = new byte[fstream.Length];
                fstream.Read(arrayRead, 0, arrayRead.Length);
            }

            FileContentResult fileContent = new FileContentResult(arrayRead, "multipart/form-data");

            System.IO.File.Delete(filesNew[0]);

            return fileContent;
        }

        [HttpPost, DisableRequestSizeLimit]
        public async void NbchPost()
        {
            string id = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString();

            string xmlData = "";
            if (Request.Body != null)
            {
                var streamReader = new StreamReader(Request.Body);
                xmlData = await streamReader.ReadToEndAsync();
            }

            if (true)
            {
                string path = @"C:\log\RKK";
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                using (FileStream stream = new FileStream($"{path}\\" + id + "_request.xml", FileMode.OpenOrCreate))
                {
                    byte[] array = Encoding.Default.GetBytes(xmlData);
                    stream.Write(array, 0, array.Length);
                }
            }
            
            try
            {
                var file = Request.Form.Files[0];
                string path = Configuration["LogingPath"] + "NBCH\\";
                if (Configuration["Logging"] == "true")
                    {
                        System.IO.File.AppendAllText(System.IO.Path.Combine(path, "PackageEmulatorLogs", "log.txt"), "NBCH DEBUG    " + file.Name + "\n");
                    }

                // записываем пришедший файл на винт
                using (var fileStream = new FileStream(path + file.Name, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
            }
            catch (Exception e)
            {
                Response.WriteAsync(e.Message);
            }
        }
    }
}