using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Text.RegularExpressions;

namespace PackageRequest.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ExperianController : ControllerBase
    {
        private readonly IConfiguration Configuration;

        public ExperianController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        [HttpPost, DisableRequestSizeLimit]
        public async void OkbList()
        {
            DateTime startEmul = DateTime.Now;
            string id = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString();

            string xmlData = "";
            if (Request.Body != null)
            {
                var streamReader = new StreamReader(Request.Body);
                xmlData = await streamReader.ReadToEndAsync();
            }

            if (bool.Parse(Configuration["LogIncomming"]))
            {
                string path = Configuration["LogingPath"] + "Experian";
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                using (FileStream stream = new FileStream($"{path}\\" + id + "_request.xml", FileMode.OpenOrCreate))
                {
                    byte[] array = Encoding.Default.GetBytes(xmlData);
                    stream.Write(array, 0, array.Length);
                }
            }

            string trimText = xmlData.Substring(xmlData.LastIndexOf("Content-Disposition: form-data; name=\"ActionFlag\"") + 51);
            string actionFlag = trimText.Substring(0, trimText.IndexOf("--"));
            string resp = "";
            if (Int32.Parse(actionFlag.Trim()) == 7)
            {
                DateTime date = DateTime.Now;

                resp = "<s>" +
                    "<s n=\"Data\">" +
                    "<a n=\"ActionFlag\">7</a>" +
                    "<c n=\"History\">" +
                    "<s>" +
                    "<s n=\"warnings\">" +
                    "<a n=\"present\">0</a>" +
                    "</s>" +
                    "<c n=\"wlRecords\"></c>" +
                    "<a n=\"wlReturnCount\">0</a>" +
                    "</s>" +
                    "</c>" +
                    "<a n=\"StreamID\">30289927</a>" +
                    "<a n=\"ValidationErrors\" />" +
                    "<a n=\"errorCode\">0</a>" +
                    "<a n=\"responseDate\">" + date.ToString("yyyyMMddhhmmss") + "</a>" +
                    "<a n=\"supportMail\" />" +
                    "</s>" +
                    "</s>";
            }
            else if (Int32.Parse(actionFlag.Trim()) == 9)
            {
                string[] files = Directory.GetFiles(Configuration["RKK_EiResponcePath"]); //Берем список файлов для ответа отсюда

                List<string> nameFiles = new List<string>();
                foreach (string s in files)
                {
                    string[] words = s.Split(new char[] { '\\' });
                    nameFiles.Add(words[2]);
                }

                nameFiles.ToArray();

                string str = "<s><a n = \"Name\">???</a></s>";
                string strXml = "";
                for (int i = 0; i < nameFiles.Count; i++)
                {
                    nameFiles[i] = str.Replace("???", nameFiles[i]);
                    strXml += nameFiles[i].ToString() + "\n";
                }

                DateTime date = DateTime.Now;

                resp = "<s>" +
                                  "<s n=\"Data\">" +
                                      "<a n=\"ActionFlag\">9</a>" +
                                      "<c n=\"Files\">" +
                                      "<s>" +
                                      "<c n=\"wlListOfFiles\">" +
                                      "???" +
                                      "</c>" +
                                      "<a n=\"wlReturnCount\">" + nameFiles.Count().ToString() + "</a>" +
                                      "</s>" +
                                      "</c>" +
                                      "<a n=\"StreamID\">30289927</a>" +
                                      "<a n=\"ValidationErrors\" />" +
                                      "<a n=\"errorCode\">0</a>" +
                                      "<a n=\"responseDate\">" + date.ToString("yyyyMMddhhmmss") + "</a>" +
                                  "</s>" +
                              "</s>";

                resp = resp.Replace("???", strXml);
            }
            else if (Int32.Parse(actionFlag.Trim()) == 1)
            {
                Regex regex = new Regex("RESP(.*?)pem.pem");
                MatchCollection matches = regex.Matches(xmlData);
                string[] files = Directory.GetFiles(Configuration["RKK_EiResponcePath"]); //Берем список файлов для ответа отсюда

                foreach (Match match in matches)
                {
                    foreach (string s in files)
                    {
                        if (s.IndexOf(match.Value) > 0)
                        {
                            byte[] arrayRead;
                            using (FileStream fstream = System.IO.File.OpenRead(Configuration["RKK_EiResponcePath"] + match.Value))
                            {
                                arrayRead = new byte[fstream.Length];
                                fstream.Read(arrayRead, 0, arrayRead.Length);
                            }
                            resp = Encoding.UTF8.GetString(arrayRead);
                        }
                    }
                }
            }
            else
            {
                DateTime date = DateTime.Now;

                resp = "<s>" +
                    "<c n=\"ValidationErrors\">" +
                    "<s>" +
                    "<a n=\"number\">503 Service Unavailable</a>" +
                    "</s>" +
                    "</c>" +
                    "<a n=\"errorCode\">1</a>" +
                    "<a n=\"responseDate\">" + date.ToString("yyyyMMddhhmmss") + "</a>" +
                    "<a n=\"streamID\">30289927</a>" +
                    "</s>";

            }
            if (bool.Parse(Configuration["LogIncomming"]))
            {
                string path =Configuration["LogingPath"] + "Experian"; //пишем лог ответа сюда
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                using (FileStream stream = new FileStream($"{path}\\" + id + "_response.xml", FileMode.OpenOrCreate))
                {
                    byte[] array = Encoding.Default.GetBytes(resp);
                    stream.Write(array, 0, array.Length);
                }
            }

            System.Threading.Thread.Sleep(int.Parse(Configuration["SleepExperian"]));

            Response.StatusCode = 200;
            Response.ContentType = "multipart/form-data";

            if (bool.Parse(Configuration["LogIncomming"]))
            {
                DateTime dateTime = DateTime.Now;
                using (StreamWriter writer = System.IO.File.AppendText(Configuration["LogingPath"] + "Experian\\OKB.csv")) //пишем лог ответа сюда
                    writer.WriteLine(dateTime.ToString("yyyy.MM.dd HH:mm:ss,fff") + ";" + dateTime.Subtract(startEmul).TotalMilliseconds.ToString());
            }

            await Response.WriteAsync(resp);
        }
    }
}