using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;
using System.IO.Compression;
using PackageRequest.Models;

namespace PackageRequest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EquifaxController : ControllerBase
    {
        public static IConfiguration AppConfiguration;
        public EquifaxController(IConfiguration configuration)
        {
            AppConfiguration = configuration;
        }

        [HttpGet]
        public void EquifaxGet ()
        {
            string path = AppConfiguration["LogingPath"];

            while (true)
            {
                byte[] resp = new byte[0];
                byte[] clearRequest = new byte[0];
                string[] filesOnFtpInbox = Directory.GetFiles(AppConfiguration["FtpDirectoryIn"]);
                string[] filesOnResponsDir = Directory.GetFiles(AppConfiguration["RKK_EquifaxResponcePath"]);
                string cleanFilename = "";
                string cleanFilenameResponce = "";
                foreach (string filename in filesOnFtpInbox)
                {
                    foreach (string filenameResponce in filesOnResponsDir)
                    {
                        // Проверка имён файлов
                        if (!filename.Contains("DYH"))
                        {
                            if (AppConfiguration["Loging"] == "true")
                            {
                                System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), filename +"\n");
                            }
                        }
                        else if (!filenameResponce.Contains("DYH"))
                        {
                            if (AppConfiguration["Loging"] == "true")
                            {
                                System.IO.File.AppendAllText(System.IO.Path.Combine(path, "log.txt"), filenameResponce +"\n");
                            }
                        }

                        else
                        {
                            cleanFilename = filename.Substring(filename.IndexOf("DYH"), filename.IndexOf(".zip") - filename.IndexOf("DYH"));
                            cleanFilenameResponce = filenameResponce.Substring(filenameResponce.IndexOf("DYH"), filenameResponce.IndexOf(".zip") - filenameResponce.IndexOf("DYH"));

                            if (filename.Length > 0 && cleanFilename == cleanFilenameResponce.Substring(0, cleanFilenameResponce.IndexOf("_out")))
                            {
                                System.IO.File.Copy(filenameResponce, AppConfiguration["FtpDirectoryOut"] + cleanFilenameResponce + ".zip.sig.enc", true);

                                System.IO.File.Delete(filename);
                            }
                        }
                    }
                }

                System.Threading.Thread.Sleep(Convert.ToInt32(AppConfiguration["SleepEquifax"]));
            }
/*
            while (true)
            {
                try{
                    ftpFileList = FtpWorker.FtpFileList(AppConfiguration["FtpAddressIn"], AppConfiguration["FtpUser"], AppConfiguration["FtpPassword"]);
                }
                catch (Exception ex)
                {
                    if (AppConfiguration["Logging"] == "true")
                    {
                        System.IO.File.AppendAllText(System.IO.Path.Combine(path, "PackageEmulatorLogs", "log.txt"), ex.Message + "\n");
                    }
                }

                if (ftpFileList.Length > 0)
                {
                    // получаем список файлов на FTP
                    fileName = ftpFileList;
                    try
                    {
                        // загружаем архив с ответом на FTP
                        FtpWorker.FtpUploadFile(AppConfiguration["FtpAddressOut"], path + "\\Equifax\\" + targetFile, AppConfiguratio["FtpUser"], AppConfiguration["FtpPassword"]);
                    }
                    catch (Exception ex)
                    {
                        if (AppConfiguration["Logging"] == "true")
                        {
                            System.IO.File.AppendAllText(System.IO.Path.Combine(path, "PackageEmulatorLogs", "log.txt"), ex.Message +"\n");
                        }
                    }  
                    System.Threading.Thread.Sleep(60000);
                }
                else
                {
                    System.Threading.Thread.Sleep(60000);
                }
            }
                
                try
                {
                    // скачаиваем файл с FTP
                    downloadFileName = FtpWorker.FtpDownloadFile(AppConfiguration["FtpAddressIn"], path + "\\Equifax\\" + fileName, AppConfiguration["FtpUser"], AppConfiguration["FtpPassword"]);
                }
                catch (Exception ex)
                {
                    if (AppConfiguration["Logging"] == "true")
                    {
                        System.IO.File.AppendAllText(System.IO.Path.Combine(path, "PackageEmulatorLogs", "log.txt"), ex.Message + "\n");
                    }
                }
            
                try
                {
                    // удаляем скачанный файл из FTP
                    FtpWorker.FtpDeleteFile(AppConfiguration["FtpAddressIn"], AppConfiguration["FtpUser"], AppConfiguration["FtpPassword"]);
                }
                catch (Exception ex)
                {
                    if (AppConfiguration["Logging"] == "true")
                    {
                        System.IO.File.AppendAllText(System.IO.Path.Combine(path, "PackageEmulatorLogs", "log.txt"), ex.Message + "\n");
                    }
                }
            
                try
                {
                    // расшифровываем скачанный архив
                    using (FileStream byteFileStream = System.IO.File.OpenRead(path + "\\Equifax\\" + fileName))
                    {
                        byte[] array = new byte[byteFileStream.Length];
                        cleanRequest = Cryptography.CryptoPro.DecryptMsg(array);
                    }
                }
                catch (Exception ex)
                {
                    if (AppConfiguration["Logging"] == "true")
                    {
                        System.IO.File.AppendAllText(System.IO.Path.Combine(path, "PackageEmulatorLogs", "log.txt"), ex.Message + "\n");
                    }
                }

                try
                {
                    // сохраняем расшифрованный файл
                    using (FileStream cleanFileStream = new FileStream(path + "\\Equifax\\clean_" + fileName, FileMode.Create))
                    {
                        cleanFileStream.Write(cleanRequest, 0, cleanRequest.Length);
                    }
                }
                catch (Exception ex)
                {
                    if (AppConfiguration["Logging"] == "true")
                    {
                        System.IO.File.AppendAllText(System.IO.Path.Combine(path, "PackageEmulatorLogs", "log.txt"), ex.Message + "\n");
                    }
                }

                try
                {
                    // распаковываем расшифрованный файл
                    string targetFile = path + "PackageEmulatorLogs\\clean_" + fileName;
                    ZipFile.ExtractToDirectory(targetFile, path);
                }
                catch (Exception ex)
                {
                    if (AppConfiguration["Logging"] == "true")
                    {
                        System.IO.File.AppendAllText(System.IO.Path.Combine(path, "PackageEmulatorLogs", "log.txt"), ex.Message + "\n");
                    }
                }

                    // архивируем файл ответа
                    PackageRequest.ZipUnZip.Compress(targetFile, path + downloadFileName.Substring(0, downloadFileName.IndexOf(".")) + ".zip");
                */
        }
    }
}